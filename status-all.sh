#!/bin/bash

HERE=$(pwd)

echo "=================================================>"

cd /workdir

pushd sources
../scripts/convenience-scripts/status.sh
popd

if [[ -d sources/meta-phy-stm-resy-collection ]]; then
   pushd sources/meta-phy-stm-resy-collection
   ../../scripts/convenience-scripts/status.sh
   popd
fi

if [[ -d sources/meta-aries-polarfire-resy-collection ]]; then
   pushd sources/meta-aries-polarfire-resy-collection
   ../../scripts/convenience-scripts/status.sh
   popd
fi

pushd scripts
../scripts/convenience-scripts/status.sh
popd

pushd app-container-multi-arch
../scripts/convenience-scripts/status.sh
popd

pushd app-container-x86-64
../scripts/convenience-scripts/status.sh
popd

pushd app-container-arm-v7
../scripts/convenience-scripts/status.sh
popd

if [[ -d app-container-aarch64 ]]; then
   pushd app-container-aarch64
   ../scripts/convenience-scripts/status.sh
   popd
fi

pushd crops-container-x86-64
../scripts/convenience-scripts/status.sh
popd

pushd oci-container-x86-64
../scripts/convenience-scripts/status.sh
popd

pushd jenkins
../scripts/convenience-scripts/status.sh
popd

if [[ -d fossology ]]; then
   pushd fossology
   ../scripts/convenience-scripts/status.sh
   popd
fi

echo "<================================================="

cd ${HERE}
