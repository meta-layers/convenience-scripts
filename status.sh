#!/bin/bash
echo "-->"
# only git status -s if there is a .git dir inside the dir - meaning it's under git
# was introduced because I grouped a couple of layers under xxx_collection
find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && [ -d ".git" ] && git status -s && echo)' \;
echo "<--"
